# OR-Tools Multiplatform

This package provides a simple multiplatform Java Maven dependency for  [Google's Operations Research tools](https://developers.google.com/optimization/).

Since the JAR contains all native libraries, it is quite large.

## Usage

### Initialization
The following snippet extracts and loads the correct native library version for your platform.

```java
cc.ilo.ortools.OrToolsMultiplatform.init();
```

### Maven Dependency
You need to add this to the ``<dependencies>`` section of your ``pom.xml``.

```xml
<dependency>
     <groupId>cc.ilo.ortools</groupId>
     <artifactId>or-tools-multiplatform</artifactId>
     <version>v2015-09</version>
</dependency>
```
### Maven Repository
You need to add this to the ``<repositories>`` section of your ``pom.xml``.

```xml
<repository>
     <id>bintray</id>
     <url>https://dl.bintray.com/nikola-ilo/maven/</url>
</repository>
```


## License

This or-tools-multiplatform and also the original or-tools are distributed under the Apache License 2.0.
